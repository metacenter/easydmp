# Generated by Django 2.2.12 on 2020-09-18 12:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dmpt', '0009_storageforecastquestion'),
    ]

    operations = [
        migrations.RenameField(
            model_name='question',
            old_name='obligatory',
            new_name='on_trunk',
        ),
    ]
